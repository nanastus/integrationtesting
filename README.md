# H1 Integration testing excercise
---

This is a school project to practice integration testing.

It has 3 endpoints:

get / for testing 

get /rgb-to-hex for changing rgb to hex
- rgb needs to be given
  - red=
  - green=
  - blue=
get /hex-to-rgb for changing hex to rgb
- hex needs to be given without #
